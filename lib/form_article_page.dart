import 'package:flutter/material.dart';
import 'package:fclient_lutter_app/article_page.dart';
import 'package:fclient_lutter_app/login_page.dart';
import 'service/login.service.dart';
import 'service/article.service.dart';
import 'model/article.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FormArticlePage extends StatefulWidget {
  static String tag = 'form-article-page';
  @override
  _FormArticlePageState createState() => new _FormArticlePageState();
}

class _FormArticlePageState extends State<FormArticlePage> {
  final titreController = TextEditingController();
  final descriptionController = TextEditingController();
  bool _estPublie = false;
  LoginService loginService = LoginService();
  ArticleService articleService = ArticleService();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();

  void _estPublieChanged(bool value) => setState(() => _estPublie = value);

  @override
  Widget build(BuildContext context) {

    final titre = TextFormField(
      keyboardType: TextInputType.text,
      autofocus: false,
      controller: titreController,
      validator: (value) {
        if (value.isEmpty) {
          return 'Donner le titre';
        }
      },
      decoration: InputDecoration(
        hintText: 'titre',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
      ),
    );

    final description = TextFormField(
      autofocus: false,
      controller: descriptionController,
      decoration: InputDecoration(
        hintText: 'description',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
      ),
    );

    final estPublie = CheckboxListTile(
      value: _estPublie,
      onChanged: _estPublieChanged,
      title: new Text('Publié larticle'),
      controlAffinity: ListTileControlAffinity.leading,
      activeColor: Colors.blue
      ,
    );

    final saveButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () {
          if (_formKey.currentState.validate()) {
            var article = Article(titreController.text, descriptionController.text, _estPublie);
            setState(() => _isLoading = true);
            articleService.postArticle(article)
                .then((data) => Navigator.of(context).pushReplacementNamed(ArticlePage.tag))
                .catchError((error) => _onPostError('Serveur injoignable'));
          }
        },
        padding: EdgeInsets.all(12),
        color: Colors.lightBlueAccent,
        child: Text('Enregistrer', style: TextStyle(color: Colors.white)),
      ),
    );

    final listLabel = FlatButton(
      child: Text(
        'Liste des articles',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () => Navigator.of(context).pushReplacementNamed(ArticlePage.tag),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text('Formulaire'),
        actions: <Widget>[
          // action button
          Hero(
            tag: 'disconnecter',
            child: GestureDetector(
              onTap: () {
                _getToken().then((token) => loginService.logout(token))
                            .then((data) => Navigator.of(context).pushReplacementNamed(LoginPage.tag));
              },
              child: ClipRRect(
                borderRadius: BorderRadius.circular(30.0),
                child: Image.asset('assets/disconnect.jpg')
              ),
            ),
          )
        ],
      ),
      backgroundColor: Colors.white,
      key: scaffoldKey,
      body: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            SizedBox(height: 48.0),
            titre,
            SizedBox(height: 8.0),
            description,
            SizedBox(height: 8.0),
            estPublie,
            SizedBox(height: 24.0),
            _isLoading ? new CircularProgressIndicator() : saveButton,
            listLabel
          ],
        ),
      ),
    );
  }

  void _onPostError(String errorTxt) {
    _showSnackBar(errorTxt);
    setState(() => _isLoading = false);
  }
  void _showSnackBar(String text) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }
  Future<String> _getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = (prefs.getString('token') ?? '') ;
    print(token);
    return token;
  }
}

