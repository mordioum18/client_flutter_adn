import 'package:http/http.dart' as http;
import 'dart:async';
import '../model/article.dart';
import '../util/constant.dart';
import 'dart:convert' as convert;
class ArticleService{
  Future<List<Article>> getArticles() async {
    print("entered");
    Map<String, String> requestHeaders = {
      'X-Parse-Application-Id':  APPLICATION_ID,
      'X-Parse-REST-API-Key': REST_API_KEY,
      'Content-Type': 'Application/json',
    };
    var uri = Uri.https(BASE_URL, '/classes/Article');
    return await http.get(uri, headers: requestHeaders)
        .then((dynamic response){
      print(response.body.toString());
      if (response.statusCode == 200) {
        var articles = new List<Article>();
        var data = convert.jsonDecode(response.body);
        for (var val in data['results']){
          articles.add(Article.fromJson(val));
        }
        return articles;
      } else {
        return Future.error(response.body);
      }
    }).catchError((error) => Future.error(error));
  }

  Future<Article> getArticleById(String id) async {
    print("entered");
    Map<String, String> requestHeaders = {
      'X-Parse-Application-Id':  APPLICATION_ID,
      'X-Parse-REST-API-Key': REST_API_KEY,
      'Content-Type': 'Application/json',
    };
    var uri = Uri.https(BASE_URL, '/classes/Article/'+id);
    return await http.get(uri, headers: requestHeaders)
        .then((dynamic response){
      print(response.body.toString());
      if (response.statusCode == 200) {
        return Article.fromJson(convert.jsonDecode(response.body));
      } else {
        return Future.error(response.body);
      }
    }).catchError((error) => Future.error(error));
  }

  Future<Object> deleteArticle(String id) async {
    print("entered");
    Map<String, String> requestHeaders = {
      'X-Parse-Application-Id':  APPLICATION_ID,
      'X-Parse-REST-API-Key': REST_API_KEY,
      'Content-Type': 'Application/json',
    };
    var uri = Uri.https(BASE_URL, '/classes/Article/'+id);
    return await http.delete(uri, headers: requestHeaders)
        .then((dynamic response){
      print(response.body.toString());
      if (response.statusCode == 200) {
        return convert.jsonDecode(response.body);
      } else {
        return Future.error(response.body);
      }
    }).catchError((error) => Future.error(error));
  }

  Future<Article> postArticle(Article article) async {
    print("service");
    Map<String, String> requestHeaders = {
      'X-Parse-Application-Id':  APPLICATION_ID,
      'X-Parse-REST-API-Key': REST_API_KEY,
      'Content-Type': 'Application/json'
    };
    var uri = Uri.https(BASE_URL, '/classes/Article');
    Map data = {'titre': article.titre, 'description': article.description, 'estPublie': false};
    return await http.post(uri, body: convert.jsonEncode(data), headers: requestHeaders)
        .then((dynamic response){
      print(response.body.toString());
      if (response.statusCode == 201) {
        var value = convert.jsonDecode(response.body);
        print(value['objectId']);
        return getArticleById(value['objectId']);
      } else {
        return Future.error(response.body);
      }
    }).catchError((error) => Future.error(error));
  }
}