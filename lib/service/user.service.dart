import 'package:http/http.dart' as http;
import 'dart:async';
import '../model/user.dart';
import '../util/constant.dart';
import 'dart:convert' as convert;
class UserService{

  Future<Object> postUser(User user) async {
    print("service");
    Map<String, String> requestHeaders = {
      'X-Parse-Application-Id':  APPLICATION_ID,
      'X-Parse-REST-API-Key': REST_API_KEY,
      'X-Parse-Revocable-Session': '1',
      'Content-Type': 'Application/json'
    };
    var uri = Uri.https(BASE_URL, '/users');
    Map data = {'username': user.email, 'email': user.email, 'password': user.password};
    return await http.post(uri, body: convert.jsonEncode(data), headers: requestHeaders)
        .then((dynamic response){
      print(response.body.toString());
      if (response.statusCode == 201) {
        return convert.jsonDecode(response.body);
      } else {
        return Future.error(response.body);
      }
    }).catchError((error) => Future.error(error));
  }
}