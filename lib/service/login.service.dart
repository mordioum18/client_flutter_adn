import 'package:http/http.dart' as http;
import 'dart:async';
import '../model/session.dart';
import '../util/constant.dart';
import 'dart:convert';
class LoginService{
  Future<Session> login(String username, String password) async {
    print("entered");
    Map<String, String> requestHeaders = {
      'X-Parse-Application-Id':  APPLICATION_ID,
      'X-Parse-REST-API-Key': REST_API_KEY,
      'Content-Type': 'Application/json',
      'X-Parse-Revocable-Session': '1'
    };
    Map<String, String> queryParameters = {
      'username': username,
      'password': password,
    };
    var uri = Uri.https(BASE_URL, '/login', queryParameters);
    return await http.get(uri, headers: requestHeaders)
        .then((dynamic response){
      print(response.body.toString());
      if (response.statusCode == 200) {
        return Session.fromJson(json.decode(response.body));
      } else {
        return Future.error(response.body);
      }
    }).catchError((error) => Future.error(error));
  }

  Future<Object> logout(String token) async {
    print("entered");
    Map<String, String> requestHeaders = {
      'X-Parse-Application-Id':  APPLICATION_ID,
      'X-Parse-REST-API-Key': REST_API_KEY,
      'X-Parse-Session-Token': token
    };
    var uri = Uri.https(BASE_URL, '/logout');
    return await http.post(uri,body: {}, headers: requestHeaders)
        .then((dynamic response){
      print(response.body.toString());
      return json.decode(response.body);
    }).catchError((error) => Future.error(error));
  }
}