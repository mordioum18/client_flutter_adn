import 'package:flutter/material.dart';
import 'model/article.dart';
import 'service/article.service.dart';
import 'package:fclient_lutter_app/form_article_page.dart';


enum ConfirmAction { CANCEL, ACCEPT }

class ArticlePage extends StatefulWidget {
  static String tag = 'list-article-page';
  @override
  _ArticlePage createState() => new _ArticlePage();
}
class _ArticlePage extends State<ArticlePage> {
  var articles = new List<Article>();
  var articleService= ArticleService();

  getarticles() {
    articleService.getArticles()
        .then((data){
      setState(() {
        articles = data;
      });
    });
  }

  initState() {
    super.initState();
    getarticles();
  }

  dispose() {
    super.dispose();
  }

  @override
  build(context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Liste'),
          actions: <Widget>[
            // action button
            IconButton(
              icon: Icon(Icons.add),
              onPressed: () =>  Navigator.of(context).pushReplacementNamed(FormArticlePage.tag),
            )
          ],
        ),
        body: ListView.builder(
          itemCount: articles.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(
                  articles[index].titre,
                  style: new TextStyle(fontWeight: FontWeight.bold)
              ),
              subtitle: Text(articles[index].description),
                onTap: () {
                _asyncConfirmDialog(context, articles[index]);
                }
            );
          },
        ));
  }

  Future<ConfirmAction> _asyncConfirmDialog(BuildContext context, Article article) async {
    return showDialog<ConfirmAction>(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Ête vous sûr de vouloir supprimer l\'article '+ article.titre +'?'),
          content: Text(
              'Vous êtes sur le point de supprimer ' + article.titre),
          actions: <Widget>[
            FlatButton(
              child: const Text('Annuler'),
              onPressed: () {
                Navigator.of(context).pop(ConfirmAction.CANCEL);
              },
            ),
            FlatButton(
              child: const Text('Supprimer'),
              onPressed: () {
                Navigator.of(context).pop(ConfirmAction.ACCEPT);
                articleService.deleteArticle(article.objectId)
                    .then((data){
                      setState(() {
                        for (var ar in articles){
                          if (ar.objectId == article.objectId){
                            articles.remove(ar);
                            break;
                          }
                        }
                      });
                });
              },
            )
          ],
        );
      },
    );
  }
}