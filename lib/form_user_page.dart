import 'package:flutter/material.dart';
import 'package:fclient_lutter_app/login_page.dart';
import 'package:email_validator/email_validator.dart';
import 'model/user.dart';
import 'service/user.service.dart';


class FormUserPage extends StatefulWidget {
  static String tag = 'form-user-page';
  @override
  _FormUserPageState createState() => new _FormUserPageState();
}

class _FormUserPageState extends State<FormUserPage> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  UserService userService = UserService();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {

    final email = TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      controller: emailController,
      validator: (val) => !EmailValidator.validate(val, true)
        ? 'Email invalide'
        : null,
      decoration: InputDecoration(
        hintText: 'Email',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
      ),
    );

    final password = TextFormField(
      autofocus: false,
      controller: passwordController,
      validator: (value) {
        if (value.isEmpty) {
          return 'Donner le mot de passe';
        }
      },
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Password',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
      ),
    );

    final registerButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () {
          if (_formKey.currentState.validate()) {
            var user = new User(emailController.text, emailController.text, passwordController.text);
            setState(() => _isLoading = true);
            userService.postUser(user)
                .then((data){
              print(data.toString());
              Navigator.of(context).pushReplacementNamed(LoginPage.tag);
            }).catchError((error) => _onRegisterError('Cette adresse existe déjà'));
          }
        },
        padding: EdgeInsets.all(12),
        color: Colors.lightBlueAccent,
        child: Text('Créer', style: TextStyle(color: Colors.white)),
      ),
    );

    return Scaffold(
      appBar: new AppBar(
        title: new Text('Création compte'),
      ),
      backgroundColor: Colors.white,
      key: scaffoldKey,
      body: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            SizedBox(height: 48.0),
            email,
            SizedBox(height: 8.0),
            password,
            SizedBox(height: 24.0),
            _isLoading ? new CircularProgressIndicator() : registerButton,
          ],
        ),
      ),
    );
  }

  void _onRegisterError(String errorTxt) {
    _showSnackBar(errorTxt);
    setState(() => _isLoading = false);
  }
  void _showSnackBar(String text) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }
}

