import 'package:flutter/material.dart';
import 'package:fclient_lutter_app/login_page.dart';
import 'package:fclient_lutter_app/article_page.dart';
import 'package:fclient_lutter_app/form_article_page.dart';
import 'package:fclient_lutter_app/form_user_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final routes = <String, WidgetBuilder>{
    LoginPage.tag: (context) => LoginPage(),
    FormArticlePage.tag: (context) => FormArticlePage(),
    ArticlePage.tag: (context) => ArticlePage(),
    FormUserPage.tag: (context) => FormUserPage(),
  };
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: LoginPage(),
      routes: routes,
      debugShowCheckedModeBanner: false,
    );
  }
}