class Session{
  String _objectId;
  String _username;
  String _createdAt;
  String _updatedAt;
  String _sessionToken;


  Session.fromJson(Map<String, dynamic> parsedJson){
    _objectId = parsedJson['objectId'];
    _username = parsedJson['username'];
    _createdAt = parsedJson['createdAt'];
    _updatedAt = parsedJson['updatedAt'];
    _sessionToken = parsedJson['sessionToken'];
  }

  String get sessionToken => _sessionToken;

  String get updatedAt => _updatedAt;

  String get createdAt => _createdAt;

  String get username => _username;

  String get objectId => _objectId;

}