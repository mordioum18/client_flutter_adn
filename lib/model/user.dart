class User{
  String _objectId;
  String _username;
  String _email;
  String _password;

  User.fromJson(Map<String, dynamic> parsedJson){
    _objectId = parsedJson['objectId'];
    _username = parsedJson['username'];
    _email = parsedJson['email'];
    _password = parsedJson['password'];
  }

  User(this._email, this._username, this._password);

  String get password => _password;

  String get email => _email;

  String get username => _username;

  String get objectId => _objectId;


}