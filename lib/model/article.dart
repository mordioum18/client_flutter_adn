class Article{
  String _objectId;
  String _titre;
  String _description;
  bool _estPuble;


  Article(this._titre, this._description, this._estPuble);

  Article.fromJson(Map<String, dynamic> parsedJson){
    _objectId = parsedJson['objectId'];
    _titre = parsedJson['titre'];
    _description = parsedJson['description'];
    _estPuble = parsedJson['estPuble'];
  }


  bool get estPuble => _estPuble;

  String get description => _description;

  String get titre => _titre;

  String get objectId => _objectId;

}