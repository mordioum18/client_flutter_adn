import 'package:flutter/material.dart';
import 'package:fclient_lutter_app/form_article_page.dart';
import 'package:fclient_lutter_app/form_user_page.dart';
import 'model/session.dart';
import 'service/login.service.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:email_validator/email_validator.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  LoginService loginService = LoginService();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'android',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('assets/android.png'),
      ),
    );

    final email = TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      //initialValue: 'mordioum@gmail.com',
      controller: emailController,
      validator: (val) => !EmailValidator.validate(val, true)
          ? 'Email invalide'
          : null,
      decoration: InputDecoration(
        hintText: 'Email',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final password = TextFormField(
      autofocus: false,
      //initialValue: 'Motdepasse',
      controller: passwordController,
      validator: (value) {
        if (value.isEmpty) {
          return 'Donner le mot de passe';
        }
      },
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Password',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () {
          if (_formKey.currentState.validate()) {
            setState(() => _isLoading = true);
            loginService.login(emailController.text, passwordController.text)
                .then((Session session){
              _saveToken(session);
              Navigator.of(context).pushReplacementNamed(FormArticlePage.tag);
            }).catchError((error) async {
              _onLoginError('Combinaison incorrecte');
            });
          }
        },
        padding: EdgeInsets.all(12),
        color: Colors.lightBlueAccent,
        child: Text('Se connecter', style: TextStyle(color: Colors.white)),
      ),
    );

    final createAccount = FlatButton(
      child: Text(
        'Créer un compte',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () => Navigator.of(context).pushNamed(FormUserPage.tag)
    );

    return Scaffold(
      appBar: new AppBar(
        title: new Text('Welcome'),
      ),
      backgroundColor: Colors.white,
      key: scaffoldKey,
      body: Center(
        child: Form(
          key: _formKey,
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              SizedBox(height: 48.0),
              email,
              SizedBox(height: 8.0),
              password,
              SizedBox(height: 24.0),
              _isLoading ? new CircularProgressIndicator() : loginButton,
              createAccount
            ],
          ),
        )
      ),
    );
  }

  void _onLoginError(String errorTxt) {
    _showSnackBar(errorTxt);
    setState(() => _isLoading = false);
  }
  void _showSnackBar(String text) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }

  _saveToken(Session session) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', session.sessionToken);
  }
}

